
export <- function() {
  files <- list.files(path="/home/alexis/Documents/automata-inference/analyse/recuit", pattern="*.csv", full.names=TRUE, recursive=FALSE)
  size <- length(files)
  for (p in 1:size) {
    x = files[p]
    t <- read.table(x ,header = TRUE, sep = " ")
    names(t) <- c("fitness")
    name = paste(c("file", p), collapse = "")
    png(file=name,
        width=600, height=350)
    hist(t$fitness, main = x)
    dev.off()
  }
}

main <- function() {
  export()
}



