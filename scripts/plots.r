
#############################################
#4

# Read file content
myfile <- read.table("Documents/M1_I2L/RO/Projet/automata-inference/analyse/rs_4_29_0.01.csv", header = TRUE, sep = " ")

# Display first lines
head(myfile)

# Rename columns
names(myfile) <- c("fitness")

# Drow histogramme
hist(myfile$fitness, main = "exemple rs_4_29_0.01.csv")

#############################################
#8

# Read file content
myfile <- read.table("Documents/M1_I2L/RO/Projet/automata-inference/analyse/rs_8_29_0.01.csv", header = TRUE, sep = " ")

# Display first lines
head(myfile)

# Rename columns
names(myfile) <- c("fitness")

# Drow histogramme
hist(myfile$fitness, main = "exemple rs_8_29_0.01.csv")


#############################################
# 16

# Read file content
myfile <- read.table("Documents/M1_I2L/RO/Projet/automata-inference/analyse/rs_16_29_0.01.csv", header = TRUE, sep = " ")

# Display first lines
head(myfile)

# Rename columns
names(myfile) <- c("fitness")

# Drow histogramme
hist(myfile$fitness, main = "exemple rs_16_29_0.01.csv")


#############################################
# 32

# Read file content
myfile <- read.table("Documents/M1_I2L/RO/Projet/automata-inference/analyse/rs_32_29_0.01.csv", header = TRUE, sep = " ")

# Display first lines
head(myfile)

# Rename columns
names(myfile) <- c("fitness")

# Drow histogramme
hist(myfile$fitness, main = "exemple rs_32_29_0.01.csv")

