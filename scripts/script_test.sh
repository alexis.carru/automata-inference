#!/bin/bash

nbRun=100

nbEvalList="10 100"

#echo random search

echo nbeval fitness > rs_test_16_15_0.1.csv
 for n in ${nbEvalList}
 do
     echo nbEval ${n}
     for((i=0; i < ${nbRun};i++))
     do
         echo -n $i' '
         ./test ${n} ${i} >> rs_test_16_15_0.1.csv
     done
     echo
 done
echo

