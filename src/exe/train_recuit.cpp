/*
  demo.cpp

 Author:
  Sebastien Verel,
  Univ. du Littoral Côte d'Opale, France.

 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimber.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

/*
    Main
*/
int main(int argc, char **argv) {

    char* strain = argv[1];
    char* stest = argv[2];
    int nbstate = atoi(argv[3]);
    int id = atoi(argv[4]);

    Sample sample(strain);
    double fitness;

    std::mt19937 gen(id);
    SmartEval seval(gen, sample);

    Solution<double> test(nbstate, 2);
    HillClimber h;
    h.search(test, sample, 500, seval);
    std::cout << test.fitness() << std::endl;


    return 0;
}