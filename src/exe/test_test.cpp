/*
  demo.cpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation 

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimber.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

/* 
    Main
*/
int main(int argc, char **argv) {

    Sample sample("../instances/dfa_16_15_0.01_train-sample.json");
    Sample sampleTest("../instances/dfa_16_15_0.01_test-sample.json");


    std::mt19937 gen(15);
    SmartEval seval(gen, sample);
    SmartEval seval2(gen, sampleTest);
    BasicEval eval(sampleTest);

    Solution<double> test(16, 2);
    HillClimber h;
    h.search(test, sample, 20000, seval);
    // cout << test << endl << endl;

    //evalue le fichier de test avec l'automate créé à partir du fichier train correspondant
    seval(test);


    std::cout << test << std::endl;


    return 0;
}