//
// Created by alexis on 20/10/2019.
//

#ifndef FSSP_HILLCLIMBER_H
#define FSSP_HILLCLIMBER_H

#include <iostream>
#include <fstream>
#include <vector>

#include <base/sample.h>
#include <base/solution.h>
#include <tuple>


#include "rapidjson/document.h"

#include <base/word.h>

class HillClimber {
public:
    HillClimber() = default;

    void search(Solution<double> &s, Sample &sample, int nbIteration, Eval<double> &eval) {
        double fitness;
        double tmp_fitness;
        eval(s);
        std::cout<<s<<std::endl;
        int stateToChange;
        int transitionToChange;
        int newValue;
       // std::cout << s << std::endl;
        int cpt = 0;
        while (cpt<nbIteration && s.fitness() < 1) {
            std::tie(stateToChange, transitionToChange, newValue) = maxStateToChange(s, sample, eval);
            //int newState = s.stateToChange(stateToChange, transitionToChange);
            if (newValue != -1 && stateToChange != -1 && transitionToChange != -1) {
                s.function[stateToChange][transitionToChange] = newValue;
                eval(s);
                //std::cout << s << std::endl;
            }
            cpt++;
        }
        std::cout<<s<<std::endl;

    }

    void recuit(Solution<double> &s, Sample &sample, int nbIteration, Eval<double> &eval, float T, float alpha) {
        eval(s);
        std::cout<<s<<std::endl;
        double fitness = s.fitness();
       // int oldState = -1;
        //int oldTransition = -1;
        int oldValue = -1;
        int newState = -1;
        int newTransition = -1;
        int newValue = -1;
        int cpt = 0;
        int same = 0;
        while (cpt < nbIteration) {
            if (same == 500) {
                break;
            }
            std::tie(newState, newTransition, newValue) = getRandomNewState(s, sample, eval);

            // oldState = newState;
           // oldTransition = newTransition;

            oldValue = s.function[newState][newTransition];

            s.function[newState][newTransition] = newValue;
            eval(s);
            float delta = s.fitness() - fitness;

            if (delta > 0) {
                cpt++;
                fitness = s.fitness();
                same = 0;
            }
            else {
                std::random_device rd;
                std::mt19937 mt(rd());
                std::uniform_real_distribution<double> dist(0.0, 1.0);
                double u = dist(mt);
                if (u<exp(delta/T)) {
                    cpt++;
                    fitness = s.fitness();
                    same=0;
                }
                else {
                    s.function[newState][newTransition] = oldValue;
                    same++;
                    //std::cout<<"####################################################################################################"<<same<<std::endl;
                }
            }
            T = T * alpha;
        }
        std::cout<<s<<std::endl;

    }


    std::tuple<int, int, int> getRandomNewState(Solution<double> &s, Sample &sample, Eval<double> &eval) {
        double fitness = s.fitness();

        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<int> dist(0, s.nStates-1);
        std::uniform_int_distribution<int> dist2(0, s.alphabetSize-1);
        std::uniform_int_distribution<int> dist3(0, s.nStates-1);

       int randomState = rand() % s.nStates;
      int  randomTransition = rand() % s.alphabetSize;
      int  newValue = rand() % s.nStates;

        return std::make_tuple(randomState, randomTransition, newValue);//renvoie un tuple avec les coordonnées à modifier avec la nouvelle valeur
    }

    std::tuple<int, int, int> maxStateToChange(Solution<double> &s, Sample &sample, Eval<double> &eval) {//BEST IMPROVEMENT
        double fitness = s.fitness();
        int stateToChange = -1;
        int transitionToChange = -1;
        int newValue = -1;
        for (int stateToTest = 0; stateToTest < s.nStates; stateToTest++) {
            for (int transitionTotest = 0; transitionTotest < s.alphabetSize; transitionTotest++) {
                int newState = s.stateToChange(stateToTest, transitionTotest);//récupère le nouvel état
                int oldState = s.function[stateToTest][transitionTotest];//sauvegarde l'ancien état
                s.function[stateToTest][transitionTotest] = newState;
                eval(s);
                if (s.fitness() > fitness) {//test la nouvelles fitness
                    stateToChange = stateToTest;
                    transitionToChange = transitionTotest;
                    newValue = newState;

                }
                s.function[stateToTest][transitionTotest] = oldState;//remet l'ancien état
                eval(s);

            }
        }
        return std::make_tuple(stateToChange, transitionToChange, newValue);//renvoie un tuple avec les coordonnées à modifier avec la nouvelle valeur
    }

    std::tuple<int, int, int> firstStateToChange(Solution<double> &s, Sample &sample, Eval<double> &eval) {//FIRST IMPROVEMENT
        double fitness = s.fitness();
        int stateToChange = -1;
        int transitionToChange = -1;
        int newValue = -1;
        for (int stateToTest = 0; stateToTest < s.nStates; stateToTest++) {
            for (int transitionTotest = 0; transitionTotest < s.alphabetSize; transitionTotest++) {
                int newState = s.stateToChange(stateToTest, transitionTotest);//récupère le nouvel état
                int oldState = s.function[stateToTest][transitionTotest];//sauvegarde l'ancien état
                s.function[stateToTest][transitionTotest] = newState;
                eval(s);
                if (s.fitness() > fitness) {//test la nouvelles fitness
                    stateToChange = stateToTest;
                    transitionToChange = transitionTotest;
                    newValue = newState;
                    return std::make_tuple(stateToChange, transitionToChange, newValue);//renvoie un tuple avec les coordonnées à modifier avec la nouvelle valeur

                }
                s.function[stateToTest][transitionTotest] = oldState;//remet l'ancien état
                eval(s);

            }
        }
        return std::make_tuple(stateToChange, transitionToChange, newValue);//renvoie un tuple avec les coordonnées à modifier avec la nouvelle valeur
    }
};

#endif //FSSP_HILLCLIMBER_H
