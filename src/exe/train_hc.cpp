/*
  demo.cpp

 Author: 
  Sebastien Verel, 
  Univ. du Littoral Côte d'Opale, France.
 
 Date:
   2019/09/03 : Version 0

***
    Demo of candidate solution evaluation 

*** To compile from automata-inference directory:

mkdir build
cd build
cmake ../src/exe
make


*** To run:
./demo

*/
#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimber.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

/* 
    Main
*/
int main(int argc, char **argv) {

    char* strain = argv[1];
    char* stest = argv[2];
    int nbstate = atoi(argv[3]);
    int id = atoi(argv[4]);

    Sample sample(strain);
    Sample sampleTest(stest);


    /* Solution<double> x(4, 2);

     cout << "Print an empty solution:" << endl;
     cout << x << endl << endl;

     BasicEval eval(sample);

     eval(x);

     cout << "Solutions after full evaluation:" << endl;
     cout << x << endl << endl;*/

    double fitness;
    //int nbRun = atoi(argv[1]);

   // for (int j = 0; j < nbRun; j++) {
        //int cpt = 0;
        std::mt19937 gen(id);
        SmartEval seval(gen, sample);
        SmartEval seval2(gen, sampleTest);
        BasicEval eval(sampleTest);

        Solution<double> test(nbstate, 2);
        HillClimber h;
        h.search(test, sample, 500, seval);
        seval2(test);
        std::cout << test.fitness() << std::endl;

   // }

    /* eval(test);

     cout << test << endl << endl;

     test.function[0][0] =  test.stateToChange(0,0);
     test.function[0][1] =  test.stateToChange(0,1);
     test.function[2][1] =  test.stateToChange(2,1);
     test.function[1][1] =  test.stateToChange(1,1);
     test.function[1][1] =  test.stateToChange(1,1);
     test.function[1][1] =  test.stateToChange(1,1);


     eval(test);

     cout << test << endl << endl;*/


    return 0;
}