#include <iostream>
#include <fstream>
#include <random>

#include <base/sample.h>
#include <base/solution.h>

#include <eval/basicEval.h>
#include <eval/smartEval.h>
#include <eval/basicBiobjEval.h>
#include <eval/smartBiobjEval.h>
#include <eval/HillClimber.h>

using namespace std;


typedef pair<double, unsigned> Fitness2;

int main(int argc, char **argv) {



    Sample sample("../instances/dfa_8_4_0.1_train-sample.json");
    Sample sampleTest("../instances/dfa_8_4_0.1_test-sample.json");


    double fitness;

    std::mt19937 gen(4);
    SmartEval seval(gen, sample);
    SmartEval seval2(gen, sampleTest);
    BasicEval eval(sampleTest);

    Solution<double> test(8,2);
    HillClimber h;
    h.search(test, sample, 50000, seval);
    seval2(test);
    std::cout << test.fitness() << std::endl;

    return 0;
}