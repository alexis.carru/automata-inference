#!/bin/bash

nbRun=50
#echo random search

for state in 4 8 16 32
do
  for id in 0 8 16 29
  do
    for ratio in 0.01 0.1 0.05
    do
      echo fitness > ../analyse/recuit/rs_recuit_${state}_${id}_${ratio}.csv
      strain=/home/alexis/Documents/automata-inference/instances/dfa_${state}_${id}_${ratio}_train-sample.json
      stest=/home/alexis/Documents/automata-inference/instances/dfa_${state}_${id}_${ratio}_test-sample.json
      echo  ${state} ${id} ${ratio}
      for i in $( seq 0 $nbRun )
        do
        ./recuit ${strain} ${stest} ${state} ${id} >> ../analyse/recuit/rs_recuit_${state}_${id}_${ratio}.csv
        done
      done
    done
done
