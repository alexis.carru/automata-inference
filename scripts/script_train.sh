#!/bin/bash

nbRun=50
#echo random search

for state in 4 8 16 32
do
  for id in {0..29}
  do
    for ratio in 0.01 0.05 0.1
    do
      echo fitness > ../analyse/rs_${state}_${id}_${ratio}.csv
      strain=/home/ines/Documents/M1_I2L/RO/Projet/automata-inference/instances/dfa_${state}_${id}_${ratio}_train-sample.json
      stest=/home/ines/Documents/M1_I2L/RO/Projet/automata-inference/instances/dfa_${state}_${id}_${ratio}_test-sample.json
      echo  ${state} ${id} ${ratio}
      for i in $( seq 0 $nbRun )
       do
        ./script ${strain} ${stest} ${state} ${id} >> ../analyse/rs_${state}_${id}_${ratio}.csv
      done
    done
  done
done
